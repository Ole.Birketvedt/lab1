package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while (true){
            System.out.println("Let's play round " + roundCounter);
            String userChoice = userChoice();
            String cpuChoice = cpuChoice();
            
            // Check who won
            if (isWinner(userChoice, cpuChoice)) {
                System.out.println("Human chose " + userChoice + ", computer chose " + cpuChoice + ". Human wins!");
                humanScore ++;
            }
            else if (isWinner(cpuChoice, userChoice)) {
                System.out.println("Human chose " + userChoice + ", computer chose " + cpuChoice + ". Computer wins!");
                computerScore ++;
            }
            else {
                System.out.println("Human chose " + userChoice + ", computer chose " + cpuChoice + ". It's a tie!");
            }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            // Ask if human wants to play again
            String continueAnswer = continuePlaying();
            if (continueAnswer.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }
        }
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    public String userChoice(){
        while (true) {
            String userChoiceInput = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (rpsChoices.contains(userChoiceInput)) {
                return userChoiceInput;
            }
            else {
            System.out.println("I don't understand " + userChoiceInput + ". Could you try again?");
            }
        }
    }

    public String cpuChoice(){
        Random random = new Random();
        String cpuChoiceRandom = rpsChoices.get(random.nextInt(rpsChoices.size()));
        return cpuChoiceRandom;
    }

    public boolean isWinner(String choice1, String choice2){
        if (choice1.equals("paper")) {
            return choice2.equals("rock");
        }
        else if (choice1.equals("rock")) {
            return choice2.equals("scissors");
        }
        else {
            return choice2.equals("paper");
        }
    }
    
    public String continuePlaying() {
        while (true){
            String continuePlayingInput = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (continuePlayingInput.equals("y")) {
                roundCounter++;
                return continuePlayingInput;
            }
            else if (continuePlayingInput.equals("n")){
                return continuePlayingInput;
            }
            else {
                System.out.println("I don't understand " + continuePlayingInput + ". Try again");
            }
        }
    }

}
